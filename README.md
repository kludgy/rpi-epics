# Raspberry Pi E-Paper Picture Tools #

### Issues ###

* Command response handling is wrong for commands issuing 'le=0x00': The response should read until a zero-byte is encountered. Last two bytes are always the command status code.
* tcm_sync() can miss /TC_BUSY edges. Implement async edge detection.
* SPI clock speed may not be optimal.
* Cabling descriptions are wrong in docs: TCM M08 /CS should connect to RPI P24 CE0, *not* P15 GPIO22.

