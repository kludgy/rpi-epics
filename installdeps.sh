#!/bin/sh -e

# www.airspayce.com/mikem/bcm2835/index.html

mkdir -p tmp
cd tmp
rm -f bcm2835-1.38.tar.gz
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.38.tar.gz 
tar zxvf bcm2835-1.38.tar.gz
cd bcm2835-1.38
./configure
make
sudo make check
sudo make install

