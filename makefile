CC=gcc
CXX=g++
RM=rm -f
CPPFLAGS=-g -I./src
LDFLAGS=-g
LDLIBS=

testpat_SRCS=./src/testpat.cpp
testpat_OBJS=$(subst .cpp,.o,$(testpat_SRCS))

png2epd_SRCS=./src/png2epd.cpp
png2epd_OBJS=$(subst .cpp,.o,$(png2epd_SRCS))

OBJS=$(testpat_OBJS) $(png2epd_OBJS)
SRCS=$(testpat_SRCS) $(png2epd_SRCS)


.PHONY: clean dist-clean

all: bin testpat png2epd

bin:
	mkdir -p bin

testpat: $(testpat_OBJS)
	$(CXX) $(CPPFLAGS) $(LDFLAGS) -o ./bin/testpat $(testpat_OBJS) $(LDLIBS) -lbcm2835

png2epd: $(png2epd_OBJS)
	$(CXX) $(CPPFLAGS) $(LDFLAGS) -o ./bin/png2epd $(png2epd_OBJS) $(LDLIBS)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

dist-clean: clean
	$(RM) *~ .dependtool

include .depend
