#include <string>
#include <stdio.h>
using namespace std;
#include "picopng.cpp"
#include "epd.h"

typedef unsigned char uint8_t;
typedef unsigned int  uint32_t;

// Returns error code 11 (data outside codetree) or 0 for success.
static int loadpngRgba(const char* fname, unsigned long& w, unsigned long& h, std::vector<uint8_t>& image)
{
    //load and decode
    std::vector<uint8_t> b;
    loadFile(b, fname);
    return decodePNG(image, w, h, b.empty() ? NULL : &b[0], (unsigned long)b.size());
}

// Return 0 if the pixel should be light, else 1 if it should be dark.
static uint8_t getp(const uint8_t* rgbas, uint32_t w, uint32_t h, uint32_t x, uint32_t y)
{
    if (x >= w || y >= h)
        return 0;   // mat value

    const uint8_t* const rgba = rgbas + y*(w*4) + (x*4);
    const uint8_t g = rgba[1];
    return g > 127 ? 0 : 1;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "No file given." << std::endl;
        return 1;
    }

    unsigned long w = 0, h = 0;
    std::vector<uint8_t> image;
    int err = loadpngRgba(argv[1], w, h, image);

    if (err != 0)
    {
        std::cerr << "Failed to load PNG file (err=" << err << "): " << argv[1] << std::endl;
        return err;
    }

    // Dump the epd image data to stdout.
    // Currently only the one format: HEADER_TC_P441_230_T0

    const int w0 = 400/8, h0 = 300;
    const uint8_t* rgbas = &image[0];
    uint8_t outData[w0*h0] = {0};

    for (int y = 0; y < h0; ++y)
    {
        for (int x = 0; x < w0; ++x)
        {
            uint8_t p8 = 0;
            for (int i = 0; i < 8; ++i)
                p8 |= getp(rgbas, w, h, x*8 + (7-i), y) << i;

            outData[y*w0+x] = p8;
        }
    }

    const int nheader = fwrite(epd::HEADER_TC_P441_230_T0, epd::HEADER_SIZE, 1, stdout);
    if (nheader != 1)
    {
        std::cerr << "Failed to write header." << std::endl;
        return 2;
    }

    const int nepd = fwrite(outData, sizeof(outData), 1, stdout);
    if (nepd != 1)
    {
        std::cerr << "Failed to write EPD image data." << std::endl;
        return 3;
    }

    return 0;
}
