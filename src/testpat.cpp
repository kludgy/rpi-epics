#include <bcm2835.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <algorithm>
#include "epd.h"

#define BCM_DEBUG (0)

static const uint8_t TC_EN   = RPI_V2_GPIO_P1_11; // GPIO17
static const uint8_t TC_CS   = RPI_V2_GPIO_P1_24; // (CE0)
static const uint8_t TC_SCK  = RPI_V2_GPIO_P1_23; // (SCLK)
static const uint8_t TC_MOSI = RPI_V2_GPIO_P1_19; // (MOSI)
static const uint8_t TC_MISO = RPI_V2_GPIO_P1_21; // (MISO)
static const uint8_t TC_BUSY = RPI_V2_GPIO_P1_13; // GPIO27

static char tcm_levelChar(uint8_t level)
{
	return (level == LOW) ? ' ' : 'H';
}

struct tcm_levels
{
	uint8_t en, cs, sck, mosi, miso, busy;

	tcm_levels(uint8_t en, uint8_t cs, uint8_t sck, uint8_t mosi, uint8_t miso, uint8_t busy)
		: en(en), cs(cs), sck(sck), mosi(mosi), miso(miso), busy(busy) {}
};

bool operator ==(const tcm_levels& x, const tcm_levels& y)
{
	return
		   x.en   == y.en
		&& x.cs   == y.cs
		&& x.sck  == y.sck
		&& x.mosi == y.mosi
		&& x.miso == y.miso
		&& x.busy == y.busy
		;
}

static void tcm_reportLevels(tcm_levels l)
{
	std::cerr
		<< __FUNCTION__ << "                                    "
		<< "   EN:" << tcm_levelChar(l.en)
		<< "   CS:" << tcm_levelChar(l.cs)
		<< "  SCK:" << tcm_levelChar(l.sck)
		<< " MOSI:" << tcm_levelChar(l.mosi)
		<< " MISO:" << tcm_levelChar(l.miso)
		<< " BUSY:" << tcm_levelChar(l.busy)
		<< std::endl;
}

static tcm_levels tcm_getLevels()
{
	return tcm_levels
		( bcm2835_gpio_lev(TC_EN  )
		, bcm2835_gpio_lev(TC_CS  )
		, bcm2835_gpio_lev(TC_SCK )
		, bcm2835_gpio_lev(TC_MOSI)
		, bcm2835_gpio_lev(TC_MISO)
		, bcm2835_gpio_lev(TC_BUSY) );
}

static bool tcm_checkLevels(tcm_levels l)
{
	const tcm_levels lx = tcm_getLevels();
	const bool ok = l == lx;

	if (!ok)
	{
		std::cerr << __FUNCTION__ << ": FAILURE. Expected:" << std::endl;
		tcm_reportLevels(l );
		std::cerr << __FUNCTION__ << ": But got:" << std::endl;
		tcm_reportLevels(lx);
	}

	return ok;
}

// After transferring a command or retrieving a result via SPI, we must wait for the
// slave to finish processing.
static void tcm_sync()
{
	bcm2835_delay(1); // T[A] -- This may need to be an async edge detection.
	while (bcm2835_gpio_lev(TC_BUSY) == LOW)
	{
		bcm2835_delayMicroseconds(30); // T[BUSY] -- May want async edge detection.
	}
	bcm2835_delay(1); // T[NS] -- Not sure how long to wait. Specs say min 2-us.
}

namespace tcon {

typedef uint16_t cstat;

const cstat EP_SW_NORMAL_PROCESSING         = 0x9000;
const cstat EP_SW_WRONG_LENGTH              = 0x6700;
const cstat EP_SW_INVALID_LE                = 0x6c00;
const cstat EP_SW_WRONG_PARAMETERS_P1P2     = 0x6a00;
const cstat EP_SW_INSTRUCTION_NOT_SUPPORTED = 0x6d00;


// Global rx buffer.
static char rxb[256] = {0};
static unsigned int rxbi = 256;
static void rx_prefetch(uint8_t count)
{
	bcm2835_spi_transfern(rxb, count);
	rxbi = 0;
}

// Read 2 bytes from rb into uint16_t, correcting endianness.
static uint16_t rx_uint16m(char* rb)
{
	const char s[2] = { rb[1], rb[0] };
	return *(const uint16_t*)s;
}

static cstat rx_cstatm(char* rb)
{
	return rx_uint16m(rb);
}

static uint16_t rx_peekcstat()
{
	return rx_cstatm(rxb);
}

static cstat rx_cstat()
{
	if (rxbi > 253)
		return 0;

	const cstat s = rx_cstatm(rxb+rxbi);
	rxbi += 2;
	return s;
}

static void rx_string(std::string& s)
{
	s.clear();
	if (rxbi > 254)
		return;

	s = rxb+rxbi;
	rxbi += s.length() + 1;
}

// Image is in EPD file format. Must be serialized in packets (pk) by making
// multiple calls to uploadImageData with new packet data each time.
// Caller must assert pklen <= 251 (255 - the 4 command bytes).
cstat uploadImageData(const char* pk, uint8_t pklen)
{
	tcm_sync();
	const char ins     = 0x20;
	const char p1      = 0x01;
	const char p2      = 0x00;
	const char lc      = pklen;
	const char lc0     = std::min(lc, (char)251);
	      char cb[256] = {0};
	           cb[0]   = ins;
	           cb[1]   = p1;
	           cb[2]   = p2;
	           cb[3]   = lc;
	std::copy(pk, pk+lc0, cb+4);
	bcm2835_spi_writenb(cb, 4+lc0);
	tcm_sync();
	rx_prefetch(2);
	return rx_cstat();
}

cstat resetDataPointer()
{
	tcm_sync();
	const char ins  = 0x20;
	const char p1   = 0x0d;
	const char p2   = 0x00;
	      char cb[] = { ins, p1, p2 };
	bcm2835_spi_writenb(cb, 3);
	tcm_sync();
	rx_prefetch(2);
	return rx_cstat();
}

cstat displayUpdate()
{
	tcm_sync();
	const char ins  = 0x24;
	const char p1   = 0x01;
	const char p2   = 0x00;
	      char cb[] = { ins, p1, p2 };
	bcm2835_spi_writenb(cb, 3);
	tcm_sync();
	rx_prefetch(2);
	return rx_cstat();
}

cstat getDeviceInfo(std::string& deviceInfo)
{
	tcm_sync();
	const char ins  = 0x30;
	const char p1   = 0x01;
	const char p2   = 0x01;
	const char le   = 0x00;
	      char cb[] = { ins, p1, p2, le };
	bcm2835_spi_writenb(cb, 4);
	tcm_sync();
	rx_prefetch(255);
	uint16_t s = rx_peekcstat();
	switch (s)
	{
	case EP_SW_WRONG_LENGTH:
	case EP_SW_INVALID_LE:
	case EP_SW_WRONG_PARAMETERS_P1P2:
		deviceInfo.clear();
		break;

	default:
		rx_string(deviceInfo);
		s = rx_cstat();
		break;
	}
	return s;
}

cstat getDeviceId(char id[20])
{
	tcm_sync();
	const char ins  = 0x30;
	const char p1   = 0x02;
	const char p2   = 0x01;
	const char le   = 0x14;
	      char cb[] = { ins, p1, p2, le };
	bcm2835_spi_writenb(cb, 4);
	tcm_sync();
	rx_prefetch(22);
	uint16_t s = rx_peekcstat();
	switch (s)
	{
	case EP_SW_WRONG_LENGTH:
	case EP_SW_INVALID_LE:
	case EP_SW_WRONG_PARAMETERS_P1P2:
		std::fill(id, id+20, 0);
		break;

	default:
		std::copy(rxb, rxb+20, id);
		s = rx_cstatm(rxb+20);
		break;
	}
	return s;
}

cstat getSystemInfo(std::string& systemInfo)
{
	tcm_sync();
	const char ins  = 0x31;
	const char p1   = 0x01;
	const char p2   = 0x01;
	const char le   = 0x00;
	      char cb[] = { ins, p1, p2, le };
	bcm2835_spi_writenb(cb, 4);
	tcm_sync();
	rx_prefetch(255);
	uint16_t s = rx_peekcstat();
	switch (s)
	{
	case EP_SW_WRONG_LENGTH:
	case EP_SW_INVALID_LE:
	case EP_SW_WRONG_PARAMETERS_P1P2:
		systemInfo.clear();
		break;

	default:
		rx_string(systemInfo);
		s = rx_cstat();
		break;
	}
	return s;
}

cstat getSystemVersionCode(char vc[16])
{
	tcm_sync();
	const char ins  = 0x31;
	const char p1   = 0x02;
	const char p2   = 0x01;
	const char le   = 0x10;
	      char cb[] = { ins, p1, p2, le };
	bcm2835_spi_writenb(cb, 4);
	tcm_sync();
	rx_prefetch(18);
	uint16_t s = rx_peekcstat();
	switch (s)
	{
	case EP_SW_WRONG_LENGTH:
	case EP_SW_INVALID_LE:
	case EP_SW_WRONG_PARAMETERS_P1P2:
		std::fill(vc, vc+16, 0);
		break;

	default:
		std::copy(rxb, rxb+16, vc);
		s = rx_cstatm(rxb+16);
		break;
	}
	return s;
}

cstat readSensorData(uint16_t& thermistor)
{
	tcm_sync();
	const char ins  = 0xe5;
	const char p1   = 0x01;
	const char p2   = 0x00;
	const char le   = 0x02;
	      char cb[] = { ins, p1, p2, le };
	bcm2835_spi_writenb(cb, 4);
	tcm_sync();
	rx_prefetch(4);
	uint16_t s = rx_peekcstat();
	switch (s)
	{
	case EP_SW_WRONG_LENGTH:
	case EP_SW_INVALID_LE:
	case EP_SW_WRONG_PARAMETERS_P1P2:
		thermistor = 0;
		break;

	default:
		thermistor = rx_uint16m(rxb);
		s = rx_cstatm(rxb+2);
		break;
	}
	return s;
}

};

namespace epd {

static void uploadP441(const char data[15000])
{
	tcon::uploadImageData(HEADER_TC_P441_230_T0, HEADER_SIZE);
	int n = 15000;
	while (n > 0)
	{
		const int l = std::min(n, 251);
		tcon::uploadImageData(data + 15000 - n, (uint8_t)l);
		n -= l;
	}
}

};

static void tcm_reportDeviceInfo()
{
	std::string di;
	const tcon::cstat s = tcon::getDeviceInfo(di);
	std::cerr << "getDeviceInfo: 0x" << std::hex << s << std::endl;
	std::cout << di << std::endl;
}

static void tcm_reportDeviceId()
{
	char id[20] = {0};
	const tcon::cstat s = tcon::getDeviceId(id);
	std::cerr << "getDeviceId: 0x" << std::hex << s << std::endl;
	for (int i = 0; i < 20; ++i)
	{
		std::cout
			<< std::setw(2) << std::setfill('0') << std::hex
			<< (unsigned int)id[i];
	}
	std::cout << std::endl;
}

static void tcm_reportSystemInfo()
{
	std::string si;
	const tcon::cstat s = tcon::getSystemInfo(si);
	std::cerr << "getSystemInfo: 0x" << std::hex << s << std::endl;
	std::cout << si << std::endl;
}

static void tcm_reportSystemVersionCode()
{
	char vc[16] = {0};
	const tcon::cstat s = tcon::getSystemVersionCode(vc);
	std::cerr << "getSystemVersionCode: 0x" << std::hex << s << std::endl;
	for (int i = 0; i < 16; ++i)
	{
		std::cout
			<< std::setw(2) << std::setfill('0') << std::hex
			<< (unsigned int)vc[i];
	}
	std::cout << std::endl;
}

// Piecewise conversion table given in developers guide.
static bool tcm_cvtThermistorCelsius(uint16_t x, float& y)
{
	float a = 0, b = 0;
	if (x > 160)
	{
		y = 0;
		return false;
	}
	else if (x > 86) { a = 0.39f; b =  -4.75f; }
	else if (x > 61) { a = 0.43f; b =  -8.55f; }
	else if (x > 41) { a = 0.52f; b = -13.95f; }
	else if (x > 29) { a = 0.66f; b = -19.69f; }
	else             { a = 0.00f, b =   0.00f; }
	y = a*x + b;
	return true;
}

static void tcm_reportSensorData()
{
	uint16_t t0 = 0;
	const tcon::cstat s = tcon::readSensorData(t0);
	float t = 0;
	const bool thermInRange = tcm_cvtThermistorCelsius(t0, t);
	std::cerr
		<< "readSensorData: 0x" << std::hex << s << "," << std::dec << t0
		<< (thermInRange ? "" : " <<out of range>>") << std::endl;
	std::cout << t << std::endl;
}

static void tcm_uploadTestPattern()
{
	const int w = 400/8;
	const int h = 300;
	char data[w*h] = {0};
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			data[y*w+x] = (y&1) ? 0xAA : 0x55; // 50% dither
		}
	}
	epd::uploadP441(data);
}

static int tcm_uploadEpdFile(const char* fname)
{
	std::ifstream in(fname, std::ios::in | std::ios::ate | std::ios::binary);
	const std::size_t len = in.tellg();
	const std::size_t expectedWidth = 400;
	const std::size_t expectedHeight = 300;
	const std::size_t expectedLen = expectedWidth/8*expectedHeight+epd::HEADER_SIZE;
	if (len != expectedLen)
	{
		std::cerr
			<< __FUNCTION__ << ": " << fname
			<< ": expected file size (" << expectedLen
			<< ") but got (" << len << ")."
			<< std::endl;
		return 1;
	}
	char data[expectedLen] = {0};
	in.seekg(0);
	in.read(data, expectedLen);
	for (int i = 0; i < epd::HEADER_SIZE; ++i)
	{
		if (data[i] == epd::HEADER_TC_P441_230_T0[i])
			continue;

		std::cerr
			<< __FUNCTION__ << ": " << fname
			<< ": header mismatch (must be HEADER_TC_P441_230_T0)." 
			<< std::endl;
	}
	epd::uploadP441(data + epd::HEADER_SIZE);
	return 0;
}

static int testpat(int argc, char* argv[])
{
	if (!bcm2835_init())
		return 1;

	// Configure additional TCM I/O pins
	bcm2835_gpio_fsel(TC_EN,   BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(TC_BUSY, BCM2835_GPIO_FSEL_INPT);

	// Cycle for startup? Is this even necessary? Voodoo mysticism!
	// What happens if the device is executing previous instructions? Will this reset it?
	bcm2835_gpio_write(TC_EN, HIGH);
	bcm2835_delay(1); // arbitrary minimal delay

	// TCM Startup
	bcm2835_gpio_write(TC_EN, LOW);
	bcm2835_delay(3 + 200); // T[STARTUP] + T[INIT] (max)
	tcm_checkLevels(tcm_levels(0,1,1,0,0,1));

	// Init SPI mode
	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE2); // CPOL=1, CPHA=0  |--_-_-_-_-_-_-_-_-|
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_4096);
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);

	// Issue commands
	tcm_reportDeviceInfo();
	tcm_reportDeviceId();
	tcm_reportSystemInfo();
	tcm_reportSystemVersionCode();
	tcm_reportSensorData();

	// Issue commands: Upload image
	int err = 0;
	if (argc < 2)
	{
		tcm_uploadTestPattern();
	}
	else
	{
		err = tcm_uploadEpdFile(argv[1]);
	}
	if (err == 0)
	{
		tcon::displayUpdate();
	}

	// Cleanup
	bcm2835_spi_end();
	bcm2835_gpio_write(TC_EN, HIGH);
	bcm2835_close();
	return err;
}

int main(int argc, char* argv[])
{
#if BCM_DEBUG
	// Use bcm2835_set_debug(1) to disable GPIO access for testing.
	bcm2835_set_debug(1);
#endif

	return testpat(argc, argv);
}
